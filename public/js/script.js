import weapons from "./weapons.js";

//TODO:
//- check if a player has been reached 5 goals and change game field for a winner declaration display.

//globals
const globalScore = {
  player: 0,
  computer: 0,
};
const weaponsToPlay = [];
const weaponsCards = [];

let roundsCounter = 0;
let numOfWeapons = 0;

// DOM
const startTemplate = document.querySelector(".start-template");
const rochambeau = document.querySelector(".rochambeau");
const rochambeauHeader = document.querySelector(".rochambeau-header");
const variantButtons = document.querySelectorAll(".variant-btn");
const scoreContainer = document.querySelector(".score-container");
const gameField = document.querySelector(".game-field");
const resetArea = document.querySelector(".reset-area");

// scoreTableLabels
const roundLabel = document.createElement("h3");
const playerScoreLabel = document.createElement("p");
const computerScoreLabel = document.createElement("p");
const actionLabel = document.createElement("p");

function getComputerChoice(variant) {
  return Math.floor(Math.random() * variant);
}

function setScore() {
  roundLabel.innerText = `Round ${roundsCounter++}`;
  playerScoreLabel.innerText = globalScore["player"];
  computerScoreLabel.innerText = globalScore["computer"];
}

function setAction(statusClass, winnerIndex, loserIndex) {
  if (statusClass !== "tie") {
    actionLabel.innerText = `${weapons[winnerIndex]["name"]} ${weapons[winnerIndex]["action"][loserIndex]} ${weapons[loserIndex]["name"]}`;
  } else {
    actionLabel.innerText = `${weapons[winnerIndex]["name"]} vs ${weapons[loserIndex]["name"]}`;
  }
  actionLabel.classList.remove(...actionLabel.classList);
  actionLabel.classList.add(statusClass);
}

function updateScore(magnitude, playerSelection, computerSelection) {
  switch (magnitude) {
    case -1:
      ++globalScore["computer"];
      setAction("loose", computerSelection, playerSelection);
      break;
    case 1:
      ++globalScore["player"];
      setAction("win", playerSelection, computerSelection);
      break;
    default:
      setAction("tie", playerSelection, computerSelection);
      break;
  }

  setScore();
}

function playRound(playerSelection) {
  const computerSelection = getComputerChoice(numOfWeapons);
  let magnitude = playerSelection - computerSelection; // gets the magnitude based on playerSelection.

  if (Math.abs(magnitude) % 2 === 0) {
    magnitude /= Math.abs(magnitude) * -1; // adjust magnitude into the range (-1, 1);
  } else {
    magnitude /= Math.abs(magnitude);
  }

  updateScore(magnitude, playerSelection, computerSelection);

  return magnitude;
}

function restartGame() {
  actionLabel.innerText = "";
  actionLabel.classList.remove(...actionLabel.classList);
  roundsCounter = 0;

  globalScore["player"] = 0;
  globalScore["computer"] = 0;
}

function resetGame() {
  rochambeauHeader.innerText = "";

  restartGame();

  weaponsToPlay.length = 0;
  weaponsCards.length = 0;
  gameField.innerHTML = "";
  scoreContainer.innerHTML = "";
  resetArea.innerHTML = "";
  startTemplate.classList.toggle("hidden");
  rochambeau.classList.toggle("hidden");
}

function createGameField(variant) {
  // Score Nodes
  const scoreTableLabel = document.createElement("div");
  const playerScoreTable = document.createElement("div");
  const playerIcon = document.createElement("img");
  const computerScoreTable = document.createElement("div");
  const computerIcon = document.createElement("img");
  // game nodes
  const instructionLabel = document.createElement("p");
  const weaponsContainer = document.createElement("div");

  const resetBtn = document.createElement("button");
  const restartBtn = document.createElement("button");

  rochambeau.classList.toggle("hidden");
  //set icons
  playerIcon.src = "./images/players/user.svg";
  playerIcon.alt = "user icon";
  playerIcon.classList.add("player-icon");

  computerIcon.src = "./images/players/computer.svg";
  computerIcon.alt = "user icon";
  computerIcon.classList.add("player-icon");

  actionLabel.classList.add("action-label");

  computerScoreLabel.classList.add("points");
  playerScoreLabel.classList.add("points");

  scoreTableLabel.classList.add("score-table");

  // create score table
  rochambeauHeader.innerText = `${variant}`;

  setScore();

  playerScoreTable.appendChild(playerIcon);
  playerScoreTable.appendChild(playerScoreLabel);

  computerScoreTable.appendChild(computerIcon);
  computerScoreTable.appendChild(computerScoreLabel);

  scoreTableLabel.appendChild(computerScoreTable);
  scoreTableLabel.append(actionLabel);
  scoreTableLabel.appendChild(playerScoreTable);

  scoreContainer.appendChild(scoreTableLabel);
  scoreContainer.appendChild(roundLabel);

  // create weapons area
  instructionLabel.classList.add("instruction-label");
  instructionLabel.innerText = "Choose your weapon";

  gameField.appendChild(instructionLabel);

  weaponsContainer.classList.add("weapons-container");

  weaponsToPlay.forEach((weapon) => {
    const newBtn = document.createElement("button");
    newBtn.value = weaponsToPlay.indexOf(weapon);
    newBtn.classList.add("weapon-btn");
    newBtn.innerHTML = weapons[parseInt(newBtn.value)]["card"];
    newBtn.addEventListener("click", (e) => {
      e.stopPropagation();
      playRound(parseInt(e.target.value));
    });
    weaponsContainer.appendChild(newBtn);
  });

  gameField.appendChild(weaponsContainer);

  // append reset/restart button to reset area
  restartBtn.innerText = "Restart";
  restartBtn.classList.add("restart-btn");
  restartBtn.addEventListener("click", () => {
    restartGame();
    setScore();
  });
  resetBtn.innerText = "Reset";
  resetBtn.classList.add("reset-btn");
  resetBtn.addEventListener("click", resetGame);

  resetArea.appendChild(resetBtn);
  resetArea.appendChild(restartBtn);
}

function setVariant(variant) {
  if (variant === "classical") {
    numOfWeapons = 3;
  }
  if (variant === "tbbt") {
    numOfWeapons = 5;
  }

  for (let i = 0; i < numOfWeapons; i++) {
    weaponsToPlay.push(weapons[i]["name"]);
    weaponsCards.push(weapons[i]["card"]);
  }

  startTemplate.classList.toggle("hidden");

  createGameField(variant);

  return numOfWeapons;
}

variantButtons.forEach((button) => {
  button.addEventListener("click", () => {
    setVariant(button.id);
  });
});
