export default {
  0: {
    name: "rock",
    card: "&#xe902",
    action: {
      2: "crushes",
      4: "crushes",
    },
  },
  1: {
    name: "paper",
    card: "&#xe901",
    action: {
      0: "covers",
      3: "disproves",
    },
  },
  2: {
    name: "scissors",
    card: "&#xe903",
    action: {
      1: "cuts",
      4: "decapitates",
    },
  },
  3: {
    name: "spock",
    card: "&#xe904",
    action: {
      2: "smashes",
      0: "vaporizes",
    },
  },
  4: {
    name: "lizard",
    card: "&#xe900",
    action: {
      3: "poisons",
      1: "eats",
    },
  },
};
